const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        require: true
    },
    account: {
        type: String,
        require: true,
        unique: true
    },
    password: {
        type: String,
        require: true,
        unique: true
    },
    updatedTime: {
        type: Date,
        default: Date.now,
        require: true
    }
});

module.exports = mongoose.model('users', userSchema);