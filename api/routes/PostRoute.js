const express = require('express');
const router = express.Router();

const PostController = require('../controllers/PostController');
const Auth = require('../middlewares/Authenticate');

// Route to /post/ to GET
router.get('/', PostController.post_get_all);

// Route to /post/:postId to GET
router.get('/:postId', PostController.post_get_post);

// Route to /post/ to POST
router.post('/', Auth.authenticate, PostController.post_create_post);

// Route to /post/:postId to PATCH
router.patch('/:postId', Auth.authenticate, PostController.post_update_post);

// Route to /post/:postId to DELETE
router.delete('/:postId', Auth.authenticate, PostController.post_delete_post);

module.exports = router;