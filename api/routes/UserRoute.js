const express = require('express');
const router = express.Router();

const UserController = require('../controllers/UserController');
const Auth = require('../middlewares/Authenticate');

// Route to /register/ to POST
router.post('/register', UserController.user_register);

// Route to /login/ to POST
router.post('/login', UserController.user_login);

// Route to /post to GET
router.get('/post', Auth.authenticate, UserController.user_get_posts_by_userId);

// Route to /changePassword to POST
router.post('/changePassword', Auth.authenticate, UserController.user_change_password);

module.exports = router;