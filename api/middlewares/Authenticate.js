const UserModel = require('../models/UserModel');

const jwt = require('jsonwebtoken');

exports.authenticate = (req, res, next) => {
    const authorizationHeader = req.headers['authorization'];
    let token;

    if (authorizationHeader) {
        token = authorizationHeader.split(' ')[1];
    }

    if (token) {
        jwt.verify(token, 'secret', (err, decoded) => {
            if (err) {
                res.status(401).json({
                    success: false,
                    message: 'Failed to authenticate'
                });
            } else {

                UserModel.findById(decoded._id)
                .select('_id')
                .then(doc => {
                    if (!doc) {
                        res.status(404).json({
                            success: false,
                            message: 'No such user'
                        });
                    }

                    req.currentUser = doc._id;
                    next();
                });
            }
        });
    } else {
        res.status(403).json({
            success: false,
            message: 'No token provided'
        })
    }
}