const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const UserModel = require('../models/UserModel');
const PostModel = require('../models/PostModel');

// API to create a user
exports.user_register = (req, res, next) => {
    let account = (req.body.account).toUpperCase();
    const query = { account: account };

    UserModel.findOne(query)
        .select(' _id name account password updatedTime ')
        .exec()
        .then((doc) => {
            if (!doc) {
                const hashedPassword = bcrypt.hashSync(req.body.password, 8);

                const user = new UserModel({
                    _id: new mongoose.Types.ObjectId(),
                    name: req.body.name,
                    account: account,
                    password: hashedPassword
                });

                user.save()
                    .then((result) => {
                        // create a token
                        const token = jwt.sign({
                            _id: result._id,
                            name: result.name,
                            account: result.account
                        }, 'secret', {
                                expiresIn: 86400 // expires in 24 hours
                            });

                        res.status(200).json({
                            success: true,
                            data: {
                                token: token
                            }
                        });
                    })
                    .catch((err) => {
                        console.log(err);

                        res.status(401).json({
                            success: false,
                            error: err
                        });
                    })
            } else {
                return res.status(401).json({
                    success: false,
                    message: "Account exists!"
                })
            }
        })
        .catch((err) => {
            console.log(err);

            res.status(500).json({
                success: false,
                error: err
            });
        });
}

// API to login
exports.user_login = (req, res, next) => {
    let account = (req.body.account).toUpperCase();

    const query = { account: account };

    UserModel.findOne(query)
        .select(' _id name account password updatedTime ')
        .exec()
        .then((doc) => {
            if (!doc) {
                return res.status(401).json({
                    success: false,
                    message: "Account is invalid!"
                })
            }

            // check if the password is valid
            const passwordIsValid = bcrypt.compareSync(req.body.password, doc.password);
            if (!passwordIsValid) {
                return res.status(401).json({
                    success: false,
                    message: "Password is incorrect!"
                })
            }

            // if user is found and password is valid
            // create a token
            var token = jwt.sign({
                _id: doc._id,
                name: doc.name,
                account: doc.account
            }, 'secret', {
                    expiresIn: 86400 // expires in 24 hours
                });

            res.status(200).json({
                success: true,
                data: {
                    token: token
                }
            });
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json({
                success: false,
                error: err
            });
        })
}

// API to get posts by userId
exports.user_get_posts_by_userId = (req, res, next) => {
    PostModel.find({ user: req.currentUser })
        .sort({ updatedTime: 'desc' })
        .select(' _id title content description user updatedTime ')
        .populate('user')
        .exec()
        .then((docs) => {
            res.status(200).json({
                success: true,
                data: docs.map(doc => {
                    return {
                        _id: doc._id,
                        title: doc.title,
                        content: doc.content,
                        description: doc.description,
                        user: doc.user.name,
                        updatedTime: doc.updatedTime,
                        request: {
                            type: 'GET',
                            url: 'http://localhost:3000/post' + doc._id
                        }
                    }
                })
            });
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json({
                success: false,
                error: err
            });
        })
}

// API to change password
exports.user_change_password = (req, res, next) => {
    UserModel.findOne({ _id: req.currentUser })
        .select(' _id name account password updatedTime ')
        .exec()
        .then((doc) => {
            if (!doc) {
                return res.status(401).json({
                    success: false,
                    message: "Account is invalid!"
                })
            }

            const hashedPassword = bcrypt.hashSync(req.body.password, 8);

            const updateOps = {
                password: hashedPassword
            };

            UserModel.findByIdAndUpdate(req.currentUser, { $set: updateOps }, { new: true })
                .exec()
                .then(result => {
                    res.status(200).json({
                        success: true,
                        message: "Password changed successfully!"
                    });
                })
                .catch((err) => {
                    console.log(err);
                    res.status(500).json({
                        success: false,
                        error: err
                    });
                })
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json({
                success: false,
                error: err
            });
        })
}