const mongoose = require('mongoose');

const PostModel = require('../models/PostModel');

// API to get all posts
exports.post_get_all = (req, res, next) => {
    PostModel.find()
        .sort({ updatedTime: 'desc' })
        .select(' _id title content description user updatedTime name ')
        .populate('user')
        .exec()
        .then(docs => {
            res.status(200).json({
                success: true,
                data: docs.map(doc => {
                    return {
                        _id: doc._id,
                        title: doc.title,
                        content: doc.content,
                        description: doc.description,
                        user: doc.user.name,
                        updatedTime: doc.updatedTime,
                        request: {
                            type: 'GET',
                            url: 'http://localhost:3000/post/' + doc._id
                        }
                    }
                })
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                success: false,
                error: err
            });
        })
}

// API to get a post
exports.post_get_post = (req, res, next) => {
    PostModel.findById(req.params.postId)
        .select(' _id title content description user updatedTime ')
        .populate('user')
        .exec()
        .then(doc => {
            res.status(200).json({
                success: true,
                data: {
                    _id: doc._id,
                    title: doc.title,
                    content: doc.content,
                    description: doc.description,
                    user: doc.user.name,
                    updatedTime: doc.updatedTime
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                success: false,
                error: err
            });
        })
}

// API to create a post
exports.post_create_post = (req, res, next) => {
    const post = new PostModel({
        _id: new mongoose.Types.ObjectId(),
        title: req.body.title,
        content: req.body.content,
        description: req.body.description,
        user: req.currentUser
    });
    post.save()
        .then(result => {
            res.status(200).json({
                success: true,
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/post/' + result._id
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                success: false,
                error: err
            });
        })
}

// API to update a post
exports.post_update_post = (req, res, next) => {
    const id = req.params.postId;

    PostModel.findById(id)
        .then((doc) => {
            if (!doc) {
                return res.status(404).json({
                    success: false,
                    message: "The post does not exist!"
                });
            }

            if (String(doc.user) !== String(req.currentUser)) {
                return res.status(401).json({
                    success: false,
                    message: "You are not allowed to modify the post!"
                });
            }


            const updateOps = {};
            for (const ops of req.body) {
                updateOps[ops.propName] = ops.value;
            }

            updateOps['updatedTime'] = new Date();

            PostModel.update({ _id: id }, { $set: updateOps })
                .exec()
                .then(result => {
                    return res.status(200).json({
                        success: true,
                        request: {
                            type: 'GET',
                            url: 'http://localhost:3000/post/' + id
                        }
                    });
                })
                .catch((err) => {
                    console.log(err);
                    res.status(500).json({
                        success: false,
                        error: err
                    });
                })
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json({
                success: false,
                error: err
            });
        })
}

// API to delete a post
exports.post_delete_post = (req, res, next) => {
    const id = req.params.postId;

    PostModel.findById(id)
        .then((doc) => {
            if (!doc) {
                return res.status(404).json({
                    success: false,
                    message: "The post does not exist!"
                });
            }

            if (String(doc.user) !== String(req.currentUser)) {
                return res.status(401).json({
                    success: false,
                    message: "You are not allowed to delete the post!"
                });
            }

            PostModel.remove({ _id: id })
                .exec()
                .then(result => {
                    return res.status(200).json({
                        success: true,
                        message: "Deleted successfully!"
                    });
                })
                .catch((err) => {
                    console.log(err);
                    res.status(500).json({
                        success: false,
                        error: err
                    });
                })
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json({
                success: false,
                error: err
            });
        })
}