const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');

const PostRoute = require('./api/routes/PostRoute');
const UserRoute = require('./api/routes/UserRoute');

// Connect to MongoDB
mongoose.connect(
    'mongodb://nodejs:nodejs@ds237610.mlab.com:37610/nodejs'
);
// On Connection
mongoose.connection.on('connected', () => {
    console.log('Connected to database');
});
// On Error
mongoose.connection.on('error', (err) => {
    console.log('Database error: ' + err);
});
mongoose.Promise = global.Promise;

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    };
    next();
});

// Routes which should handle requests
app.use('/api/post', PostRoute);
app.use('/api/user', UserRoute);

// Set Static Folder
 app.use(express.static(path.join(__dirname, 'build')));

 app.get('/', (req, res) => {
     res.sendFile(path.join(__dirname, './build/index.html'));
 })

// Handing errors
app.use((req, res, next) => {
    const err = new Error('File Not Found');
    err.status = 404;
    next(err);
});

app.use((err, req, res, next) => {
    console.log(err);
    res.status(500).json({
        success: false,
        error: err
    });
});

module.exports = app;
